# Lucas Kanade Spore Test

This repository implements OpenCV's Lucas Kanade algorithm and applies it to a
spore example

## Usage

### Lucas Kanade
```
python3 optical_flow.py [video]
```

# Dense Optical Flow: Farneback
```
python3 optical_flow_dense.py [video]
```
